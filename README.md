# IDLV - Indien dans la ville

Ceci est un thème pour la plateforme de blog [Ghost](https://ghost.org), réalisé pour l'association Indiens dans la ville.

Il est implémenté par dessus [le thème par défaut de ghost](https://github.com/TryGhost/Casper).

## Comment l'utiliser ?

### Avoir une installation en local de ghost

```bash
nvm use 18
npx install ghost
npx ghost start
```

### Générer le thème

Pour utiliser ce thème, il faut installer les dépendances en local, puis construire un fichier .zip qui sera envoyé à Ghost. Vous pouvez utiliser les commandes suivantes :

```bash
npm install
npm run zip
```

### Travailler en local

Si vous travaillez en local, il est possible de faire un lien symbolique vers ce dossier pour vous faciliter la vie, puis de lancer la commande `yarn dev` pour que vos changements soient reflétés au fil de l'eau:


```
# Dans le dossier de l'installation ghost
ln -s ../../../ghost-theme-idlv content/themes/idlv

# Dans le dossier du thème
yarn dev
```

## Crédits

Le thème est une adaptation du travail fourni par [Arthur Masson](https://gitlab.com/arthur.sw/idlv-hugo), lui même basé sur le travail de Munif Tanjim avec [le thème minimio pour Hugo](https://github.com/MunifTanjim/minimo/).
